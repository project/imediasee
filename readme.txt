********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Imediasee Flash Video Streaming Service
Author: Rich Calosso <rich at imediasee dot com>
Drupal: 5
********************************************************************
PREREQUISITES:
 None

********************************************************************
DESCRIPTION:

This module allows you to easily add streaming video that is hosted
at the imediasee.com flash video streaming service to any node
by adding an Drupal input filter. 

The module uses the Imedisee Premium Player which is hosted by
Imediasee.  If you want to use the Imediasee streaming service with 
your own player, you don't need this module.  Instead, follow the
instructions on the Player support pages at imediasee.com.

This module is only useful if you are hosting your videos at 
Imediasee.com.  

********************************************************************
INSTALLATION:

1. Place the entire upload_progress directory into your Drupal 
   modules directory (normally sites/all/modules or modules).

2. Enable the upload_progress module by navigating to:

     Administer > Site building > Modules

3. Give permission to administer the module here:

     Administer > User management > Access Control

4. Configure the module, including the defualt values for the
   simple and extended parameters here:

     Administer > Site configuration > Imediasee Premium Player

5. Add the Imediasee input filter to the appropriate Drupal input
   formats here:
   
     Administer > Site configuration > Input Formats
	 Configure each input format that should use the filter
   
6. Add video to your nodes using the [ImediaseeVideo] tag. Follow
   the instructions in the input filter help.
********************************************************************