<?php

define('VIDEO_CCK_IMEDIASEE_MAIN_URL', 'http://www.imediasee.com/');
define('VIDEO_CCK_IMEDIASEE_TAG', '[ImediaseeVideo');

/**
 * hook video_cck_PROVIDER_info
 * this returns information relevant to a specific 3rd party video provider
 * @return
 *   an array of strings requested by various admin and other forms
 *   'name' => the translated name of the provider
 *   'url' => the url to the main page for the provider
 *   'settings_description' => a description of the provider that will be posted in the admin settings form
 *   'supported_features' => an array of rows describing the state of certain supported features by the provider.
 *      These will be rendered in a table, with the columns being 'Feature', 'Supported', 'Notes'.
 */
function video_cck_imediasee_info() {
	$name = t('Imediasee Flash Video Streaming Service');
	$features = array(
	array(t('Autoplay'), t('Yes'), ''),
	array(t('RSS Attachment'), t('No'), ''),
	array(t('Thumbnails'), t('Yes'), t('')),
	);
	return array(
    'provider' => 'imediasee',
    'name' => $name,
    'url' => VIDEO_CCK_IMEDIASEE_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !provider.', array('!provider' => l($name, VIDEO_CCK_IMEDIASEE_MAIN_URL, array('target' => '_blank')))),
    'supported_features' => $features,
	);
}

/**
 * hook video_cck_PROVIDER_settings
 * this should return a subform to be added to the video_cck_settings() admin settings page.
 * note that a form field will already be provided, at $form['PROVIDER'] (such as $form['imediasee'])
 * so if you want specific provider settings within that field, you can add the elements to that form field.
 */
function video_cck_imediasee_settings() {
	$form['imediasee']['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded Video Player Settings'),
    '#description' => t('Set default values to be used to display video.'),
    '#collapsible' => true,
    '#collapsed' => true,
	);
	$form['imediasee']['settings'] = array_merge($form['imediasee']['settings'], ifvss_standard_settings('premium'));
	return $form;
}

/**
 * this is a wrapper for video_cck_request_xml, not sure what this does
 */
/*
 function video_cck_imediasee_request($method, $args = array(), $cached = TRUE) {
 $request = module_invoke('emfield', 'request_xml', 'imediasee', VIDEO_CCK_YOUTUBE_REST_ENDPOINT, $args, $cached);
 return $request;
 }
 */

/**
 * hook video_cck_PROVIDER_extract
 * this is called to extract the video code from a pasted URL or embed code.
 * @param $embed
 *   an optional string with the pasted URL or embed code
 * @return
 *   either an array of regex expressions to be tested, or a string with the video code to be used
 *   if the hook tests the code itself, it should return either the string of the video code (if matched), or an empty array.
 *   otherwise, the calling function will handle testing the embed code against each regex string in the returned array.
 */
function video_cck_imediasee_extract($embed = '') {

	// src="http://www.youtube.com/v/nvbQQnvxXDk"
	// http://youtube.com/watch?v=nvbQQnvxXDk
	// http://www.youtube.com/watch?v=YzFCA-xUc8w&feature=dir
	// '/\[ImediaseeVideo.+flv\s*=\s*["|' . "'" . '](.+)["|' . "'" . ']\s*\]/',
	if (! empty($embed) && substr_compare($embed, VIDEO_CCK_IMEDIASEE_TAG, 0, strlen(VIDEO_CCK_IMEDIASEE_TAG)) == 0) {
		$matches = array();
		$embed = preg_replace("|\[ImediaseeVideo ([\w]+)\s*\]|i", '[ImediaseeVideo flv="$1"]', $embed);
		
		if (preg_match('/\bflv\s*=\s*["|' . "\'" . '](\w+)["|' . "\'" . ']\s*/i', $embed, $matches) == 0) {
			drupal_set_message("ImediaseeVideo did not find flv attribute:$embed");
			return FALSE;
		}
		else {
			$video_name = $matches[1];
			$customer_uid =  variable_get('ifvss_premium_customer_uid', '');
			$matches = array();
			if (preg_match('/\bcustomer_uid\s*=\s*["|' . "\'" . '](\w+)"|' . "\'" . ']\s/i', $embed, $matches) != 0) {
				$customer_uid = $matches[1];
			}
			return $customer_uid . '/' . $video_name;
		}
	}
	return FALSE;
}

/**
 * hook video_cck_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site
 *  @param $video_code
 *    the string containing the video to watch
 *  @return
 *    a string containing the URL to view the video at the original provider's site
 */
function video_cck_imediasee_embedded_link($video_code) {
	return 'imediasee_em/preview/' . $video_code;
}

/**
 * the embedded flash displaying the youtube video
 */
/*
 function theme_video_cck_imediasee_flash($embed, $width, $height, $autoplay) {

 $output = 'HELP';
 drupal_set_message('them_falsh - Embed:' . $embed);
 return $output;
 }
 */

/**
 * hook video_cck_PROVIDER_thumbnail
 * returns the external url for a thumbnail of a specific video
 * TODO: make the args: ($embed, $field, $item), with $field/$item provided if we need it, but otherwise simplifying things
 *  @param $field
 *    the field of the requesting node
 *  @param $item
 *    the actual content of the field from the requesting node
 *  @return
 *    a URL pointing to the thumbnail
 */
function video_cck_imediasee_thumbnail($field, $item, $formatter, $node, $width, $height) {
	
	$f_value = $item['embed'];
	$matches = array();
	if (preg_match('/\bimage\s*=\s*["|' . "\'" . '](\w+)["|' . "\'" . ']\s*/i', $embed, $matches) != 0) {
		return $matches[1];
	}
	return FALSE;
}

/**
 * hook video_cck_PROVIDER_video
 * this actually displays the full/normal-sized video we want, usually on the default page view
 *  @param $embed
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function video_cck_imediasee_video($embed, $width, $height, $field, $item, $autoplay) {
	//drupal_set_message('video:' . print_r($item,TRUE));

	$f_value = $item['embed'];
	$f_value = preg_replace("|\[ImediaseeVideo ([\w]+)\s*\]|i", '[ImediaseeVideo flv="$1"]', $f_value);
	$f_value = '[ImediaseeVideo width="' . $width . '" height="' . $height . '" autostart="' .
	($autoplay? 'TRUE' : 'FALSE') . '" ' . substr($f_value, 15);
	//drupal_set_message('value:' . $f_value);
	return imediasee_player_script($f_value);
}

/**
 * hook video_cck_PROVIDER_preview
 * this actually displays the preview-sized video we want, commonly for the teaser
 *  @param $embed
 *    the video code for the video to embed
 *  @param $width
 *    the width to display the video
 *  @param $height
 *    the height to display the video
 *  @param $field
 *    the field info from the requesting node
 *  @param $item
 *    the actual content from the field
 *  @return
 *    the html of the embedded video
 */
function video_cck_imediasee_preview($embed, $width, $height, $field, $item, $autoplay) {
	drupal_set_message('Preview Embed:' . $embed);
	return video_cck_imediasee_video($embed, $width, $height, $field, $item, $autoplay);
}
