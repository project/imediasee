<?php
/*
 * Created on Jan 22, 2008
 *
 */
 define(IFVSS_ADMIN, 'administer imediasee player');
 define(IFVSS_PREMIUM, 'premium');
 define(IFVSS_BASIC, 'basic');
 define(IFVSS_API, 'api');
 
/**
* Return the standard set of serivce settings.
*/
function ifvss_standard_settings($service_level) {

	$form = array();
	$var_name = 'ifvss_' . $service_level . '_customer_uid';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, ''),
		'#title' => t('Customer UID'),
		'#required' => TRUE,
		'#description' => t('Customer UID of your account on !link', array('!link' => l('imediase.com.', 'http://www.imediasee.com', array('target' => '_blank')))),
	);
	$var_name = 'ifvss_height';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, '240'),
		'#title' => t('Display Height'),
		'#required' => TRUE,
		'#validate' => array('ifvss_validate_numeric' => array(0, 2000)),
		'#description' => t('Default height of player window'),
	);
	$var_name = 'ifvss_width';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, '320'),
		'#title' => t('Display Width'),
		'#required' => TRUE,
		'#validate' => array('ifvss_validate_numeric' => array(0, 2000)),
		'#description' => t('Default width of player window'),
	);
	$var_name = 'ifvss_' . $service_level . '_autostart';
	$form[$var_name] = array(
		'#type' => 'checkbox',
		'#default_value' => variable_get($var_name, FALSE),
		'#title' => t('Autostart FLV'),
		'#description' => t('Automatically start playing the FLV file.'),
	);
	$var_name = 'ifvss_' . $service_level . '_allowfullscreen';
	$form[$var_name] = array(
		'#type' => 'checkbox',
		'#default_value' => variable_get($var_name, FALSE),
		'#title' => t('Full Screen Mode'),
		'#description' => t('Allows the player to enable full screen mode.'),
	);

	$var_name = 'ifvss_' . $service_level . '_displayheight';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, ''),
		'#title' => t('Video display height'),
		'#description' => t('Specify height of video play back window, or leave blank for full height.'),
	);

	$var_name = 'ifvss_' . $service_level . '_frontcolor';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, ''),
		'#title' => t('Front color'),
		'#description' => t('Specify front color number, or leave blank for default(#ffffff).'),
	);

	$var_name = 'ifvss_' . $service_level . '_backcolor';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, ''),
		'#title' => t('Back color'),
		'#description' => t('Specify background color number, or leave blank for default(#000000).'),
	);

	$var_name = 'ifvss_' . $service_level . '_lightcolor';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, ''),
		'#title' => t('Light color'),
		'#description' => t('Specify color number used for rollover/active state, or leave blank for default(#000000).'),
	);

	$var_name = 'ifvss_' . $service_level . '_screencolor';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, ''),
		'#title' => t('Screen color'),
		'#description' => t('Specify color number of the display background, or leave blank for default(#000000).'),
	);

	$var_name = 'ifvss_' . $service_level . '_javascript';
	$form[$var_name] = array(
		'#type' => 'checkbox',
		'#default_value' => variable_get($var_name, FALSE),
		'#title' => t('Javascript Callbacks'),
		'#description' => t('Enable javascript callbacks.'),
	);

	$var_name = 'ifvss_' . $service_level . '_repeat';
	$form[$var_name] = array(
		'#type' => 'checkbox',
		'#default_value' => variable_get($var_name, FALSE),
		'#title' => t('Repeat'),
		'#description' => t('Start playing movie over again at the end.'),
	);

	$var_name = 'ifvss_' . $service_level . '_volume';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, -1),
		'#title' => t('Volume'),
		'#description' => t('Specify start up volume, -1 does not alter volume.'),
	);

	$var_name = 'ifvss_' . $service_level . '_image';
	$form[$var_name] = array(
		'#type' => 'textfield',
		'#default_value' => variable_get($var_name, ''),
		'#title' => t('Image'),
		'#description' => t('Specify url to use for image to display if Autostart FLV is not checked, leave blank for no image'),
	);

	return $form;
}

/**
 * Field validate function to validate numberic values
 */
function ifvss_validate_numeric($element, $min = null, $max = null) {
	if (! is_numeric($element['#value'])) {
		form_error($element, t('Value must be numeric'));
	}
	$number = $element['#value'] + 0;
	if (isset($min)) {
		if ($number < $min) {
			form_error($element, t('Number out of bounds, must be greater than %value', array('%value' => $min)));
		}
	}
	if (isset($max)) {
		if ($number > $max) {
			form_error($element, t('Number out of bounds, must be less than %value', array('%value' => $max)));
		}
	}
}

/**
 * script output for imediaseePlayerObject
 */
function imediasee_player_script($im_video) {
	static $count = 0;
	$attribute_number = array('displayheight', 'height', 'width', 'volume');
	$attribute_boolean = array('autostart', 'allowfullscreen', 'javascript', 'repeat');
	$attribute_string = array('flv', 'frontcolor', 'backcolor', 'lightcolor', 'screencolor', 'customer_uid', 'image');
	$attributes = $attribute_number;
	$attributes = array_merge($attributes, $attribute_boolean);
	$attributes = array_merge($attributes, $attribute_string);
	$values = array();

	// load with settings default
	foreach ($attributes as $a_key) {
		$setting = variable_get('ifvss_premium_' . $a_key, null);
		if (isset($setting)) {
			if (is_string($setting)) {
				if (!empty($setting)) {
					$values[$a_key] = $setting;
				}
			}
			else {
				$values[$a_key] = $setting;
			}
		}
	}
	$values['height'] = variable_get('ifvss_height', '240');
	$values['width'] = variable_get('ifvss_width', '320');
	
	//parse tag attributes
	$tag_attribute = array();
	$attrib_count = preg_match_all('|(\w+)\s*=\s*["' . "'" . '](.*?)["' . "'" . ']|i', $im_video, $tag_attribute);
	if ($attrib_count > 0) {
		for ($index = 0; $index < $attrib_count; $index++) {
			$tag_name = $tag_attribute[1][$index];
			if (in_array($tag_name, $attributes)) {
				$value = $tag_attribute[2][$index];
				$values[$tag_name] = $value;
			}
			else {
				drupal_set_message("Attribute $tag_name is not recognized for ImediaseeVideo");
			}
		}
	}
	else {
		drupal_set_message('No atributes found:' . $im_video);
	}
	
	$var_name = 'ifvss_premium_' . $count++;
	$var_declare = 'var ' . $var_name . '= new imediaseePlayerObject(' .
			'"'. (! isset($values['customer_uid']) ? '<NOT SET>' : $values['customer_uid']) . '", ' .
			'"'. $values['flv'] .'", ' .
			$values['height'] . ', ' .
			$values['width'] .
			');';
	$var_setting = array();
	foreach ($attribute_boolean as $a_key) {
		$a_value = $values[$a_key];
		if (isset($a_value)) {
			$var_setting[] = $var_name . '.' . $a_key . '=' . (strcasecmp($a_value, 'FALSE') == 0 || $a_value === 0 ? 'false' : 'true') . ';';
		}
	}
	foreach ($attribute_string as $a_key) {
		$a_value = $values[$a_key];
		if (isset($a_value)) {
			$var_setting[] = $var_name . '.' . $a_key . '="' . $a_value . '";';
		}
	}
	foreach ($attribute_number as $a_key) {
		$a_value = $values[$a_key];
		if (isset($a_value)) {
			$var_setting[] = $var_name . '.' . $a_key . '=' . ($a_value + 0) . ';';
		}
	}
	$var_write = $var_name . ".write();";
	$ret = '<script>' . $var_declare . implode(' ', $var_setting) . $var_write . '</script>';
	return $ret;
}

/**
 * Implements hook_filter_tips().
 * 
 */
function imediasee_player_filter_tips($delta, $format, $long = FALSE) {
	if ($long) {
		return t('<p>Allows for simple insertion of Imediasee Flash Video Streaming Service Premium player into content</p>' .
				'<p>Two forms are allowed, simple and extended</p>' .
				'<h3>Simple</h3>' .
				'<p>Simple format allows only a video name and used the default setting for size</p>' .
				'<p>[ImediaseeVideo &lt;video name&gt;]</p>' .
				'<h3>Extended</h3>' .
				'<p>The extended vesion allows you to specify more options as configured on the extended options section or override them as attributes on the tag itself.</p>' .
				'<p>[ImediaseeVideo flv=&quot;&lt;video name&gt;&quot; ... ]</p>' .
				'<table><thead><tr><th>Atribute</th><th>Description</th></tr></thead><tbody>' .
				'<tr class="odd"><td>flv</td><td>Name of the video to play (required)</td></tr>' .
				'<tr class="even"><td>autostart</td><td>TRUE/FALSE, start playing video when loaded</td></tr>' .
				'<tr class="odd"><td>allowfullscreen</td><td>TRUE/FALSE, allow player to display full screen</td></tr>' .
				'<tr class="even"><td>displayheight</td><td>Sets the video playback height if different from player height</td></tr>' .
				'<tr class="odd"><td>frontclolor</td><td>Color number used to display buttons and text</td></tr>' .
				'<tr class="even"><td>backcolor</td><td>Color number used for the background color</td></tr>' .
				'<tr class="odd"><td>lightcolor</td><td>Color number used for rollover/active state</td></tr>' .
				'<tr class="even"><td>screencolor</td><td>Color number of the display background</td></tr>' .
				'<tr class="odd"><td>javascript</td><td>Enable javascript callbacks</td></tr>' .
				'<tr class="even"><td>height</td><td>Height of video player, overrides default height if present</td></tr>' .
				'<tr class="odd"><td>width</td><td>Width of video player, overrides default width if present</td></tr>' .
				'<tr class="even"><td>repeat</td><td>TRUE/FALSE, causes the player to start the movie over when it reached the end</td></tr>' .
				'<tr class="odd"><td>image</td><td>URL to image to display if autostart is FALSE at the beginning and also at end if repeat is FALSE</td></tr>' .
				'<tr class="even"><td>volume</td><td>Starting volume of video, -1 does not change volume</td></tr>' .
		'</tbody></table>');
	}
	return t('Insert Imediasee Flash Premium Player, default with !simple, or extened with !extended', array('!simple'=> '[ImediaseeVideo &lt;Video Name&gt;]', '!extended' => '[ImediaseeVideo flv="video_name" ...]'));
}

/**
 * Create form that allows extended attributes to be samples
 *
 * @return array
 */
function ifvss_extened_wizard() {
	$form = array();
	$form['customer_uid'] = array(
		'#type' => 'textfield',
		'#title' => t('Customer UID'),
		'#description' => t('Customer UID of your account on !link, defaults to %cust_uid if blank', array('!link' => l('imediase.com.', 'http://www.imediasee.com', array('target' => '_blank')), '%cust_uid' => variable_get('ifvss_premium_customer_uid', ''))),
	);
	$form['flv'] = array(
		'#type' => 'textfield',
		'#title' => t('Video Name'),
		'#required' => TRUE,
		'#description' => t('Enter Video Name'),
	);
	$form['height'] = array(
		'#type' => 'textfield',
		'#title' => t('Display Height'),
		'#validate' => array('ifvss_validate_numeric' => array(0, 2000)),
		'#description' => t('Height of player window, defaults to %h', array('%h' => variable_get('ifvss_height', '240'))),
	);
	$form['width'] = array(
		'#type' => 'textfield',
		'#title' => t('Display Width'),
		'#validate' => array('ifvss_validate_numeric' => array(0, 2000)),
		'#description' => t('Width of player window, defaults to %w', array('%w' => variable_get('ifvss_width', '320'))),
	);
	$var_name = 'ifvss_' . $service_level . '_autostart';
	$form['autostart'] = array(
		'#type' => 'select',
		'#title' => t('Autostart FLV'),
		'#options' => array('' => 'Default', 'TRUE' => 'Yes', 'FALSE' => 'No'),
		'#description' => t('Automatically start playing the FLV file, defaults to %d.', array('%d' => (variable_get('ifvss_premium_autostart', FALSE) ? 'Yes' : 'No'))),
	);
	$var_name = 'ifvss_' . $service_level . '_allowfullscreen';
	$form['allowfullscreen'] = array(
		'#type' => 'select',
		'#title' => t('Full Screen Mode'),
		'#options' => array('' => 'Default', 'TRUE' => 'Yes', 'FALSE' => 'No'),
		'#description' => t('Enable full screen mode, defaults to %d.', array('%d' => (variable_get('ifvss_premium_allowfullscreen', FALSE) ? 'Yes' : 'No'))),
	);

	$form['displayheight'] = array(
		'#type' => 'textfield',
		'#title' => t('Video display height'),
		'#description' => t('Specify height of video play back window, or leave blank for default.  Defaults to %d', array('%d' => (variable_get('ifvss_premium_displayheight', '') == '' ? 'blank' : variable_get('ifvss_premium_displayheight', '')))),
	);

	$form['frontcolor'] = array(
		'#type' => 'textfield',
		'#title' => t('Front color'),
		'#description' => t('Specify front color number or leave blank for default.  Defaults to %d', array('%d' => (variable_get('ifvss_premium_frontcolor', '') == '' ? 'blank' : variable_get('ifvss_premium_frontcolor', '')))),
	);

	$form['backcolor'] = array(
		'#type' => 'textfield',
		'#title' => t('Back color'),
		'#description' => t('Specify back color number or leave blank for default.  Defaults to %d', array('%d' => (variable_get('ifvss_premium_backcolor', '') == '' ? 'blank' : variable_get('ifvss_premium_backcolor', '')))),
	);

	$form['lightcolor'] = array(
		'#type' => 'textfield',
		'#title' => t('Light color'),
		'#description' => t('Specify color number used for rollover/active state or leave blank for default.  Defaults to %d', array('%d' => (variable_get('ifvss_premium_lightcolor', '') == '' ? 'blank' : variable_get('ifvss_premium_lightcolor', '')))),
	);

	$form['screencolor'] = array(
		'#type' => 'textfield',
		'#title' => t('Screen color'),
		'#description' => t('Specify color number used for the display background or leave blank for default.  Defaults to %d', array('%d' => (variable_get('ifvss_premium_screencolor', '') == '' ? 'blank' : variable_get('ifvss_premium_screencolor', '')))),
	);

	$form['javascript'] = array(
		'#type' => 'select',
		'#title' => t('Javascript Callbacks'),
		'#options' => array('' => 'Default', 'TRUE' => 'Yes', 'FALSE' => 'No'),
		'#description' => t('Enable javascript callbacks, defaults to %d.', array('%d' => (variable_get('ifvss_premium_javascript', FALSE) ? 'Yes' : 'No'))),
	);

	$form['repeat'] = array(
		'#type' => 'select',
		'#default_value' => variable_get($var_name, FALSE),
		'#title' => t('Repeat'),
		'#options' => array('' => 'Default', 'TRUE' => 'Yes', 'FALSE' => 'No'),
		'#description' => t('Enable playing movie over again at the end, defaults to %d.', array('%d' => (variable_get('ifvss_premium_repeat', FALSE) ? 'Yes' : 'No'))),
	);

	$form['volume'] = array(
		'#type' => 'textfield',
		'#title' => t('Volume'),
		'#description' => t('Specify start up volume, -1 does not alter volume or leave blank for default.  Defaults to %d', array('%d' => variable_get('ifvss_premium_volume', '-1'))),
	);

	$form['image'] = array(
		'#type' => 'textfield',
		'#title' => t('Image'),
		'#description' => t('Specify url to use for image to display if Autostart FLV is not checked, leave blank for default.  Defaults to %d',  array('%d' => (variable_get('ifvss_premium_image', '') == '' ? 'blank' : variable_get('ifvss_premium_image', '')))),
	);

	return $form;
}
?>
