/* Drupal secure stub
 * Contains functions to display placeholder for imediaseePlayer flash player
 * 
 */

var imediaseePlayerPath;

function imediaseePlayerWrapper(customer,title,height,wide)
{
	var ipObj = new imediaseePlayerObject(customer,title,height,wide);
	ipObj.write();
	return ipObj;
}

function imediaseePlayer_getColorInt(color) {
	var colorInt = 0;
	if ( typeof (color) == "string") {
		if (color.substr(0,1) == '#' ) {
			var colorHex = '0x' + solor.substr(1);
			colorInt = colorHex + 0;
		} else {
			colorInt = color + 0;
		}
	} else {
		colorInt = color + 0;
	}
	return colorInt;
}

function imediaseePlayer_getColorHTML(color) {
	var colorInt = imediaseePlayer_getColorInt(color);
	var longColor = "000000" + colorInt.toString(16).substr(2);
	return "#" + longColor.substr(-6);
}

function imediaseePlayer_play()
{
	send('playerEvent');
}

function imediaseePlayerObject_html(divName)
{
	var errMessage = "";
	var customer = this.customer_uid.toUpperCase();
	
	if ( customer == "" ) {
		errMessage += "Missing Customer ID<P>";
	}
	if ( this.video == "" ) {
		errMessage += "Missing Title Name<P>";
	}
	if ( this.height == "" ) {
		errMessage += "Missing height<P>";
	}
	if ( this.width == "" ) {
		errMessage += "Missing width<P>";
	}

	var dHeight = this.height;
	if ( this.displayheight != null )
		dHeight = this.displayheight;
		
	var playerServer = this.server;
	if (this.playerServer != null )
		playerServer = this.playerServer;

	var bgcolor = "#000000";
	if (this.screencolor != null) {
		bgcolor = imediaseePlayer_getColorHTML(this.screencolor);
	}

	if ( errMessage == "" ) {
		document.getElementById(divName).innerHTML = "<img src='" + imediaseePlayerPath + "/imediaseePlayer.jpg' width='" + this.width  + "' height='" + this.height + "' />";  
	} else {		
		document.getElementById(divName).innerHTML = errMessage;
	}
}

function imediaseePlayerObject_getIdName() {
	return "imPWrapper_" + this.video.replace(/\s+/,"_");
}

function imediaseePlayerObject_write()
{
	var divName = "DIV_" + this.getIdName();
	// write div for player output
	document.write('<div id="' + divName + '">&nbsp;</div>');
	this.html(divName);
}

function imediaseePlayerObject(customer,video,height,wide)
{
	this.customer_uid = customer;
	this.video = video;
	this.height = height;
	this.width = wide;
}

new imediaseePlayerObject('X','X',1,1);
imediaseePlayerObject.prototype.write = imediaseePlayerObject_write;
imediaseePlayerObject.prototype.html = imediaseePlayerObject_html;
imediaseePlayerObject.prototype.getIdName = imediaseePlayerObject_getIdName;



